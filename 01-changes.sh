#! /bin/sh
#
# 01-changes.sh
# Copyright (C) 2021 Vintage Salt <rehashedsalt@cock.li>
#
# Distributed under terms of the MIT license.
#

echo "Recived arguments: $@"
for dir in $@; do
	[ -z "$dir" ] && continue
	echo "Syncing in directory: $dir"
	rsync -aHS "$dir" ./
done

