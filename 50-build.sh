#! /bin/sh
#
# 50-build.sh
# Copyright (C) 2021 Vintage Salt <rehashedsalt@cock.li>
#
# Distributed under terms of the MIT license.
#

set -e
# First make sure we have required environment variables
[ -n "$PACKDEV_FILENAME" ] || exit 1
suffix="latest"
if [ -n "$CI_COMMIT_REF_NAME" ]; then suffix="$CI_COMMIT_REF_NAME"; fi

# Get our build in here
echo "Syncing in modpack"
mkdir "$PACKDEV_FILENAME"
rsync -aHS ../pack/ "$PACKDEV_FILENAME"/

# And we build up the structure for our MultiMC pack
echo "Building MultiMC pack"
mv "$PACKDEV_FILENAME" minecraft
mkdir "$PACKDEV_FILENAME"
cp ../src/instance.cfg "$PACKDEV_FILENAME"/
cp ../src/mmc-pack.json "$PACKDEV_FILENAME"/
mv minecraft "$PACKDEV_FILENAME"/
rsync -aHS "$PACKDEV_FILENAME"/ ../build

echo "Done!"
