# packdev

A set of tools for janky modded-Minecraft modpack development

## Usage

### Setup

For the best results -- since this is just a collection of small shell scripts -- you should add this repository as a submodule to your current repo. Clone/fork as desired.

### 1. Downloading Mods

The ideal state is to simply have a ton of links to JAR files located in a file somewhere. Simply feed those into `00-download.sh`:

```bash
cat ../src/modlist.txt | ../packdev/00-download.sh
```

Those will be downloaded into the `./mods` directory.

### 2. Syncing Changes

After the mods are downloaded, you'll likely want to bundle in a bunch of configuration files for the mods in question. The script `01-changes.sh` will sync all of its arguments into the current directory. Like so:

```bash
../packdev/01-changes.sh ../src/config/ ../src/resourcepacks/
```

**NOTE**: This script does a pretty blind rsync as its operative step. Make damn sure you specify those trailing slashes where required.

### 3. Building the Pack

Now that you have more or less the root of a new modpack sorted out, it's time to finally build some consuamble modpack files. At present, this script is able to build one type of pack:

* MultiMC pack archives

To kick this build off, you'll want to ensure the following things:

1. Your environment is `cd`'d into an empty working directory

2. Your target directory for build artifacts is `../build` from there

3. Your pack root directory is `../pack` from there

3. The files `instance.cfg` and `mmc-pack.json` reside in `../src`

4. The following environment variables are set:

```bash
PACKDEV_FILENAME=mypack
```

Then you can finally kick the script off. It'll work its magic and you'll get a nice clean build directory with everything a multimc pack zip needs. These still need to be zipped (this in itself is a workaround for gitlab-runner issue 1955).
