#! /bin/sh
#
# 00-download.sh
# Copyright (C) 2021 Vintage Salt <rehashedsalt@cock.li>
#
# Distributed under terms of the MIT license.
#

set -e
# Make the mods directory if it doesn't exist
[ -d "./mods" ] || mkdir -p "mods"
while read line; do
	# Skip over empty lines and comments
	[ -z "$line" ] && continue
	[ "$line" == "${line#\#}" ] || continue
	# And download our fancy mod
	echo "Downloading mod: $line"
	wget "$line" -P "./mods" > /dev/null 2>&1
done

